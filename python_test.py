import platform, re
print(platform.python_version())
##Program 1

string = input("Please enter a string to reverse:")
print(string[::-1])

##Program 2
string = input("Please enter a string to reverse the words:")
words = string.split()
words = words[::-1]
print(" ".join(words))

##Program 3
string = input("Please enter a string:")

if re.findall(r"-r", string, re.IGNORECASE):
    res = string.replace('-r', '')
    print(res[::-1])
elif re.findall(r"-w", string, re.IGNORECASE):
    words = string.replace('-w', '')
    words = string.split()
    words = string[::-1]
    print(" ".join(words))
else:
    print("Please enter -r or -w")

##Program 3 with out importing re
string = input("Please enter a string:")
string = string.split("-")
string_ = ''.join(i for i in string[0] if i not in '"')
_string = ''.join(i for i in string[1] if i not in '"')
if _string.lower() == "r":
    print(string_[::-1])
elif _string.lower() == "w":
    print(string_[::-1])
else:
    print("Please enter -r or -w")

##Program 4
filename = input("Please enter file name : ")
fp = open(filename)
print(fp.read())
string = fp.read()
fp.close()
if re.findall(r"-r", string, re.IGNORECASE):
    res = string.replace('-r', '')
    print(res[::-1])
elif re.findall(r"-w", string, re.IGNORECASE):
    words = string.replace('-w', '')
    words = string.split()
    words = string[::-1]
    print(" ".join(words))
else:
    print("Please enter -r or -w")




